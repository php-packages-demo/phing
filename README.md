# phing

Build tool based on Apache Ant. https://packagist.org/packages/phing/phing

[![PHPPackages Rank](http://phppackages.org/p/phing/phing/badge/rank.svg)](http://phppackages.org/p/phing/phing)
[![PHPPackages Referenced By](http://phppackages.org/p/phing/phing/badge/referenced-by.svg)](http://phppackages.org/p/phing/phing)

* https://phing.info/

## Unofficial documentation
* [*Build tool PHP packages*](https://phppackages.org/s/Build%20tool)

### About Ant
* [*Apache Ant*](https://en.m.wikipedia.org/wiki/Apache_Ant) (Wikipedia)

### Tutorial
* [phing tutorial](https://google.com/search?q=phing+tutorial)
* (fr)[*Automatisation des tâches avec Phing*
  ](http://blog.lepine.pro/php/automatisation-des-taches-avec-phing)
  2012-11
* [*Automate Your Life with Phing*
  ](https://www.lullabot.com/articles/automate-your-life-with-phing)
  2012-02 Sally Young
* [*Using Phing, the PHP Build Tool*
  ](https://www.sitepoint.com/using-phing/)
  2012-01 Shameer C
